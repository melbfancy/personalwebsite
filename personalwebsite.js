// package importation
var express = require('express');
var exphbs  = require('express-handlebars');
var formidable = require('formidable');

var hbs = exphbs.create({
	defaultLayout: 'main',
    // Specify helpers which are only registered on this instance. 
    helpers: {
        section: function(name, options){ 
        	if(!this._sections) this._sections = {}; 
        	this._sections[name] = options.fn(this); 
        	return null;
        }
    }
});
 
// app configuration
var app = express();
app.set('port', process.env.PORT || 3000);
app.use(express.static(__dirname + '/public')); 
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

// routes
app.get('/', function(req, res){ 
	res.render('home');
});

app.post('/contact', function(req, res){
	var form = new formidable.IncomingForm();
	form.parse(req, function(err, fields, files){
		if(err) return res.redirect(303, '/');
		console.log(fields);
        res.redirect(303, '/');
    });
});

// 404 catch-all handler (middleware)
app.use(function(req, res, next){ 
	res.status(404);
	res.render('404');
});

// 500 error handler (middleware)
app.use(function(err, req, res, next){ 
	console.error(err.stack);
	res.status(500);
	res.render('500');
});

app.listen(app.get('port'), function(){
	console.log( 'Express started on http://localhost:' + app.get('port') + '; press Ctrl-C to terminate.' );
});